var testData = (function() {

    var am = 'am';
    var pm = 'pm';
    var TASK = 'task';
    var IMAGE = 'image';
    var DOCUMENT = 'document';

    var MAX_COLOR_GROUPS = 6;

    function getColorGroup(key) {
        var hash = 0;
        if (key.length) {
            for (var i = 0, l = key.length; i < l; i++) {
                hash <<= 31;
                hash += key.charCodeAt(i);
            }
        }
        return 'color-group-' + ((((hash % MAX_COLOR_GROUPS) + MAX_COLOR_GROUPS) % MAX_COLOR_GROUPS) + 1);
    }

    function createIdGenerator(prefix) {
        var i = 0;
        return function() {
            return prefix + (++i);
        };
    }

    function getAgeText(age) {
        return age === 1 ? '1 day ago' : age + ' days ago';
    }

    function getTimeText(time) {
        var s = time.hours + ':';
        if (time.minutes < 10) {
            s += '0';
        }
        s += time.minutes;
        s += time.period;
        return s;
    }

    function increaseTime(time, duration) {
        var newTime = {
            hours: time.hours,
            minutes: time.minutes,
            period: time.period
        };
        newTime.minutes += duration;
        while (newTime.minutes >= 60) {
            newTime.minutes -= 60;
            newTime.hours++;
        }
        newTime.hours = newTime.hours % 12;
        if (newTime.hours < (time.hours % 12)) {
            newTime.period = time.period == am ? pm : am;
        }
        if (newTime.hours === 0) {
            newTime.hours = 12;
        }
        return newTime;
    }

    var types = {
        person: function(id, displayName) {
            this.id = id;
            this.displayName = displayName;
        },
        day: function(id, name, events) {
            this.id = id;
            this.isWeekend = name.indexOf('S') === 0;
            this.isToday = name.indexOf('Thurs') === 0;
            this.name = name;
            this.events = events || [];
        },
        event: function(id, title, startHours, startMinutes, startPeriod, duration, attendees, location, description) {
            this.id = id;
            this.title = title;
            this.startHours = startHours;
            this.startMinutes = startMinutes;
            this.startPeriod = startPeriod;
            this.offset = ((startHours % 12) + (startPeriod === pm ? 12 : 0)) * 60 + startMinutes;
            var start = {
                hours: startHours,
                minutes: startMinutes,
                period: startPeriod
            };
            this.timeText = getTimeText(start) + ' - ' + getTimeText(increaseTime(start, duration));
            this.duration = duration;
            this.colorGroup = getColorGroup(title);
            this.attendees = attendees;
            this.location = location;
            this.description = description;
        },
        activity: function(id, type, title, age, description) {
            this.id = id;
            this.type = type;
            this.isImage = type === IMAGE;
            this.title = title;
            this.age = age;
            this.ageText = getAgeText(age);
            this.description = description;
        },
        searchResult: function(id, type, title, age) {
            this.id = id;
            this.type = type;
            this.isImage = type === IMAGE;
            this.title = title;
            this.age = age;
            this.ageText = 'Updated ' + getAgeText(age);
        }
    };

    (function() {
        for (var prefix in types) {
            if (Object.prototype.hasOwnProperty.call(types, prefix)) {
                types[prefix] = (function(delegate, idGenerator) {
                    var ctor = function() {
                        var args = Array.prototype.slice.call(arguments);
                        args.unshift(idGenerator());
                        delegate.apply(this, args);
                    };
                    ctor.prototype = delegate.prototype;
                    return ctor;
                })(types[prefix], createIdGenerator(prefix));
            }
        }
    })();

    var data = {};
    data.people = [
        new types.person('Bob Jones'),
        new types.person('Alice Smith'),
        new types.person('Scott Hall'),
        new types.person('Alana Anderson')
    ];
    data.days = [
        new types.day('Sunday'),
        new types.day('Monday'),
        new types.day('Tuesday', [
            new types.event('All staff company meeting', 1, 0, pm, 90, [
                data.people[0],
                data.people[1]
            ], 'Meeting hall', 'All staff must attend. Talks on latest mission to the moon will be given../')
        ]),
        new types.day('Wednesday'),
        new types.day('Thursday', [
            new types.event('Weekly meeting with Mining Corp.', 10, 0, am, 90, [
                data.people[0],
                data.people[1]
            ], 'Meeting room 23.d', 'Meeting with Mining Corporation to discuss progress on designs. Must urgently discuss DrillX project'),
            new types.event('Team meeting', 1, 0, pm, 60, [
                data.people[1],
                data.people[3]
            ], 'Meeting room 11.a', 'Team meeting to discuss outcome of weekly meeting with Mining Corp.'),
            new types.event('Senior engineer forum', 3, 0, pm, 60, [
                data.people[0],
                data.people[3]
            ], 'Meeting room 15.b', 'Discuss common issues across our various projects and how we can better solve them')
        ]),
        new types.day('Friday', [
            new types.event('Business lunch', 12, 0, pm, 60, [
                data.people[1],
                data.people[2]
            ], 'Pub', 'Discuss the weeks work over a tasty beverage')
        ]),
        new types.day('Saturday')
    ];
    data.activity = [
        new types.activity(DOCUMENT, 'Updated Robo v26 evaluation report', 1, 'Incorporated Sally\'s feedback'),
        new types.activity(TASK, 'Completed TODO task', 1, 'Talk to Sally about evaluation results'),
        new types.activity(TASK, 'Completed work task ROB-123', 2, 'Evaluated Robo v26'),
        new types.activity(IMAGE, 'Created Robo v27 design diagram', 3),
        new types.activity(IMAGE, 'Updated Robo v26 design diagram', 4),
        new types.activity(DOCUMENT, 'Created Robo v26 evaluation report', 6),
        new types.activity(IMAGE, 'Created Schematics for DrillZ', 7),
        new types.activity(IMAGE, 'Updated Schematics for DrillY', 7),
        new types.activity(TASK, 'Completed work task ROB-111', 7, 'Complete Schematics fir Drill V')
    ];

    data.searchResults = [
        new types.searchResult(IMAGE, 'DrillX Schematics', 5),
        new types.searchResult(IMAGE, 'DrillX Design Diagram', 6),
        new types.searchResult(DOCUMENT, 'DrillX Requirements Specification', 10),
        new types.searchResult(DOCUMENT, 'DrillX Design v22', 10),
        new types.searchResult(TASK, 'ROB-433: Create Schematic Diagram for DrillX', 11),
        new types.searchResult(IMAGE, 'DrillX Prototype', 13),
        new types.searchResult(DOCUMENT, 'DrillX Test results 1', 13),
        new types.searchResult(DOCUMENT, 'DrillX Test data', 13),
        new types.searchResult(TASK, 'ROB-334: Update Dimensions of DrillX', 14)
    ];

    return data;
})();