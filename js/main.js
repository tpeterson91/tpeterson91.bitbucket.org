;
(function($, Handlebars, testData) {

    function prettyNotes(notes) {
        var $container = $(document.createElement('div'));
        var $list;
        _.forEach(notes.split('\n'), function(line) {
            if (line.indexOf(' *') === 0) {
                if (!$list) {
                    $list = $(document.createElement('ul'));
                }
                $(document.createElement('li')).text(line.substring(2)).appendTo($list);
            } else {
                if ($list) {
                    $container.append($list);
                    $list = null;
                }
                $(document.createElement('p')).text(line).appendTo($container);
            }
        });
        if ($list) {
            $container.append($list);
            $list = null;
        }
        return $container.html();
    }

    Handlebars.registerHelper('eventOffset', function(offsetInMin) {
        var offsetInPx = Math.floor(offsetInMin * 80 / 60) + 1;
        return offsetInPx && (offsetInPx + 'px');
    });

    Handlebars.registerHelper('eventDuration', function(durationInMin) {
        var durationInPx = Math.floor(durationInMin * 80 / 60) - 1;
        return durationInPx && (durationInPx + 'px');
    });

    Handlebars.registerHelper('prettyNotes', prettyNotes);

    function idMatcher(id) {
        return function(item) {
            return item.id === id;
        };
    }

    function findEvent(id) {
        return _.chain(testData.days)
            .pluck('events')
            .flatten()
            .filter(idMatcher(id))
            .value()[0];
    }

    function addNoteOrMedia(event, contentItem) {
        if (!contentItem) {
            return;
        }
        var isSearchResult = contentItem.id.indexOf('searchResult') !== -1;
        if (contentItem.type === 'image' || (isSearchResult && contentItem.type !== 'task')) {
            // media
            var $container;
            if (event.media) {
                event.media.push(contentItem);
                $container = $('.event-summary .media > .media-items');
            } else {
                event.media = [contentItem];
                $container = $('.event-summary .media > em');
            }
            $container.replaceWith(partials.mediaItems(event));
        } else {
            // personal note
            var noteAddition = ' * ' + contentItem.title;
            if (contentItem.description) {
                noteAddition += ': ' + contentItem.description;
            }
            if (event.personalNotes) {
                event.personalNotes += '\n' + noteAddition;
                $('.event-summary .personal-notes > .notes').html(prettyNotes(event.personalNotes));
            } else {
                event.personalNotes = noteAddition;
                $('.event-summary .personal-notes > em').replaceWith(
                    $(document.createElement('div'))
                        .addClass('notes')
                        .html(prettyNotes(event.personalNotes))
                );
            }
        }

    }

    function showImagePreview(activityOrDocumentId) {
        $page.append(partials.imagePreview({
            id: activityOrDocumentId
        }));
    }

    var pages = {
        '#calendarOverview': function() {
            if (!testData.days) {
                testData.days = []
            }
            return { days: testData.days };
        },
        '#eventDetails': function(id) {
            var matches = idMatcher(id);
            return _.reduce(testData.days, function(memo, day) {
                if (!memo) {
                    var event = _.find(day.events, matches);
                    if (event) {
                        memo = {
                            day: $.extend(true, {}, day, {
                                events: _.map(day.events, function(event) {
                                    return matches(event) ? $.extend(true, {}, event, {
                                        selected: true
                                    }) : event;
                                })
                            }),
                            event: event
                        };
                    }
                }
                return memo;
            }, null);
        },
        '#contentCollection': function(eventId, mode) {
            var event = findEvent(eventId);
            var data = {
                event: event,
                isSearchMode: 'search' === mode,
                isRecentMode: 'recent' === mode
            };
            if (data.isRecentMode) {
                data.recentActivityItems = testData.activity;
            }
            return data;
        },
        '#editNotes': function(eventId) {
            return findEvent(eventId);
        }
    };

    var transitions = {
        '#calendarOverview': {
            '#eventDetails': function(fallback) {
                var scrollTop = $page.scrollTop();
                fallback();
                $('.calendar.side-panel > section').scrollTop(scrollTop);
            }
        },
        '#eventDetails': {
            '#eventDetails': function(fallback) {
                var scrollTop = $('.calendar.side-panel > section').scrollTop();
                fallback();
                $('.calendar.side-panel > section').scrollTop(scrollTop);
            },
            '#calendarOverview': function(fallback) {
                var scrollTop = $('.calendar.side-panel > section').scrollTop();
                fallback();
                $page.scrollTop(scrollTop);
            }
        },
        '#default': {
            '#eventDetails': function(fallback, id) {
                fallback();
                var event = findEvent(id);
                $('.calendar.side-panel > section').scrollTop((event.offset * 80 / 60) - 5);
            },
            '#calendarOverview': function(fallback) {
                fallback();
                $page.scrollTop(9 * 80);
            }
        }
    };

    (function() {
        for (var key in pages) {
            if (Object.prototype.hasOwnProperty.call(pages, key)) {
                $(key).html();
                pages[key] = (function(delegate, template) {
                    return function() {
                        var context = delegate.apply(this, arguments);
                        return template(context);
                    }
                })(pages[key], Handlebars.compile($(key + '-template').html().trim()));
            }
        }
    })();

    var $page = $('#page');

    function refreshPage() {
        var hashParts = (location.hash || '#').split('/');
        var pageId = hashParts.shift();
        var page = pages[pageId];
        if (!page) {
            location.hash = '#calendarOverview';
            return;
        }

        var fullRender = function() {
            $page.removeClass()
                .addClass(pageId.substring(1))
                .html(page.apply(null, hashParts));
        };

        var previousPageId = '#' + $page.attr('class');
        if (!(transitions[previousPageId] && transitions[previousPageId][pageId])) {
            previousPageId = '#default';
        }
        if (previousPageId && transitions[previousPageId] && transitions[previousPageId][pageId]) {
            var args = hashParts.slice(0);
            args.unshift(fullRender);
            transitions[previousPageId][pageId].apply(null, args);
        } else {
            fullRender();
        }
    }

    $(document).on('click', '.event-entry', function() {
        $('.event-entry.selected').removeClass('selected');
        $(this).addClass('selected');
    });

    var partials = {};
    $('script[id$="-partial"]').each(function() {
        var $this = $(this);
        var id = $this.attr('id');
        id = id.substring(0, id.length - 8);
        partials[id] = Handlebars.compile($this.html().trim());
        Handlebars.registerPartial(id, partials[id]);
    });

    var $dropZone = $(document.createElement('div')).addClass('dropzone').text('Drag here');


    $(document).on('touchmove', '.content-item > img', function(e) {
        if (e.originalEvent.touches.length === 1) {
            if (!$dropZone.parent().length) {
                $dropZone.appendTo('#page');
            }
            e.preventDefault();
            var touch = e.originalEvent.touches[0];
            $(touch.target).css({
                position: 'fixed',
                top: (touch.pageY - 30) + "px",
                left: (touch.pageX - 30) + "px"
            });
        }
    });

    $(document).on('touchend', '.content-item > img', function(e) {
        if (e.originalEvent.changedTouches.length === 1) {
            e.preventDefault();
            e.stopImmediatePropagation();
            $dropZone.detach();
            var changedTouch = e.originalEvent.changedTouches[0];
            var $target = $(changedTouch.target);
            var activityId = $target.closest('.content-item').attr('data-id');
            $target.removeAttr('style');
            if (changedTouch.pageX <= 250 && changedTouch.pageY >= 40) {
                var eventId = $('.event-summary').attr('data-id');
                addNoteOrMedia(findEvent(eventId), findContentItem(activityId));
            }
        }
    });

    $(document).on('dragstart', '.content-item > img', function(e) {
        if (!$dropZone.parent().length) {
            $dropZone.appendTo('#page');
        }
        e.originalEvent.dataTransfer.effectAllowed = 'move';
        e.originalEvent.dataTransfer.setData('activityId', $(e.target).closest('.content-item').attr('data-id'));
    });

    $(document).on('dragover', function(e) {
        e.preventDefault();
    });

    function findContentItem(activityId) {
        return _.find(
            activityId.indexOf('searchResult') !== -1 ? testData.searchResults : testData.activity,
            idMatcher(activityId)
        );
    }

    $(document).on('drop', function(e) {
        e.stopPropagation();
        e.preventDefault();
        if ($(e.target).is($dropZone)) {
            var eventId = $('.event-summary').attr('data-id');
            var activityId = e.originalEvent.dataTransfer.getData('activityId');
            addNoteOrMedia(findEvent(eventId), findContentItem(activityId));
        }
    });

    $(document).on('dragend', '.content-item > img', function() {
        $dropZone.detach();
    });

    $(document).on('input', 'input[type="search"]', function(e) {
        var term = $(e.target).val().trim().toLowerCase();
        var results = _.filter(testData.searchResults, function(doc) {
            return doc.title.toLowerCase().indexOf(term) !== -1;
        });
        if (!term) {
            $('.content').empty();
        } else if (results.length) {
            var $content = $(document.createElement('section')).addClass('content');
            _.forEach(results, function(result) {
                $content.append(partials.activityItem(result));
            });
            $content.replaceAll('.content');
        } else {
            $('.content').html($(document.createElement('em')).addClass('no-results').text('No matching results'))
        }
    });

    $(document).on('keypress', 'input[type="search"]', function(e) {
        if (e.which === 13) {
            $(e.target).blur();
        }
    });

    var lastX = 0;
    var lastY = 0;

    $(document).on('touchstart', function(e) {
        if (e.originalEvent.touches.length === 1) {
            var touch = e.originalEvent.touches[0];
            lastX = touch.pageX;
            lastY = touch.pageY;
        }
    });

    $(document).on('touchend', '.content-item.image', function(e) {
        if (e.originalEvent.changedTouches.length === 1) {
            var touch = e.originalEvent.changedTouches[0];
            var dx = Math.abs(touch.pageX - lastX);
            var dy = Math.abs(touch.pageY - lastY);
            if (dx < 5 && dy < 5) {
                e.preventDefault();
                e.stopImmediatePropagation();
                showImagePreview($(touch.target).attr('data-id'));
            }
        }
    });

    $(document).on('click', '.content-item.image', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        showImagePreview($(e.target).attr('data-id'));
    });

    $(document).on('click', '.close-image-button', function(e) {
        e.preventDefault();
        $(e.target).closest('.image-preview').remove();
    });

    $(document).on('click', '.add-image-button', function(e) {
        e.preventDefault();
        var id = $(e.target).attr('data-id');
        var eventId = $('.event-summary').attr('data-id');
        addNoteOrMedia(findEvent(eventId), findContentItem(id));
        $(e.target).closest('.image-preview').remove();
    });

    $(document).on('input', 'textarea', function(e) {
        var $this = $(e.target);
        var event = $this.data('event');
        if (!event) {
            event = findEvent($this.attr('data-id'));
            $this.data('event', event);
        }
        event.personalNotes = $this.val();
    });

    window.onhashchange = refreshPage;
    refreshPage();
})(jQuery, Handlebars, testData);