git cheat sheet

* git add . (prepare all your changes for committing)
* git commit -m "<message goes here>" (commit your code locally)
* git fetch (retrieve the latest code from the server)
* get merge origin/master (integrate your local changes with the changes on the server)
* git push (send the changes to the server)